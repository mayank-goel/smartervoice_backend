package com.smartervoice.automation.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmazonAutoPurchaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(AmazonAutoPurchaseApplication.class, args);
	}
}
