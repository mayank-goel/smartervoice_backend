package com.smartervoice.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.smartervoice.dao.LoggedUserDao;
import com.smartervoice.models.User;
import com.smartervoice.services.ILoginServiceController;

@RestController
@RequestMapping("/login")
public class LoginServiceControllerImpl implements ILoginServiceController{
	
	@Autowired
	private LoggedUserDao loggedUserDao;

	@Override
	@RequestMapping("/verifyuser")
	public String checkCredentials(String username, String password) {	
		User user = loggedUserDao.findByEmail(username, password);
		System.out.println("hi"); 
		return null;
	}

}
