package com.smartervoice.services;

public interface ILoginServiceController {
	
	String  checkCredentials(String username, String passWord);

}
