package com.smartervoice.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.smartervoice.models.User;

@Transactional
public interface LoggedUserDao extends CrudRepository<User, String> {
	

	  /**
	   * This method will find an User instance in the database by its email.
	   * Note that this method is not implemented and its working code will be
	   * automagically generated from its signature by Spring Data JPA.
	   */
	public User findByEmail(String email, String password);

}
